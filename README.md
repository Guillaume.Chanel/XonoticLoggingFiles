# Intall Config

To use this logging system just replace the original qcsrc folder in: 
xonotic\data\xonotic-data.pk3dir\qcsrc with the one in this repository.

To enable the event logging, edit the file data/xonotic-data.pk3dir/xonotic-server.cfg as follows:
set sv_eventlog 1

qcsrc/server/miscfunctions.qc contains support for LSL, it is commented out by default

# Code Reference For Parsing

This is a reference for the logging codes we obtain from Xonotic just to make sure once analysing the markers everything is correct.

All logs can be parsed by the ':' separator. The first is always a string event which dictates the rest of the marker.

Other information for the logging system can be found on the [Xonotic page](https://github.com/xonotic/xonotic/wiki/Event-logging).

## Keyword Event:

### Join
:join:PlayerID:TeamID:IPAddress:UserName
Example -- :join:3:3:37.6.246.199:XonoticPlayer159

### Team
:team:TeamID:?:?
Example -- :team:3:5:1

### Fire
:fire:PlayerID:WeaponID:PlayerCoordinates
Example -- :fire:1:2:'-1115.8 135.7 344.0'

### Item_Pickup
:item_pickup:ItemID:PlayerID:ItemQuantity:PlayerCoordinates
Example -- :item_pickup:4:2:80:'2369.9 -382.8 611.9'

### Weapon_Pickup
:weapon_pickup:PlayerID:WeaponID:PlayerCoordinates
Example -- :weapon_pickup:2:3:'2369.9 -382.8 611.9'

### Damage
:dmg:PlayerID_SHOOTER:PlayerID_VICTIM:DamageQuantity:PlayerCoordinates_SHOOTER:PlayerCoordinates_VICTIM
Example -- :dmg:1:2:79.625000:'1008.0 -1087.9 600.0':'1010.1 -861.0 600.0' 

### Self Damage
:self_dmg:PlayerID:PlayerID:DamageQuantity:PlayerCoordinates
Example -- :self_dmg:1:1:49.334324:'1182.7 -1018.4 600.0'

### Respawn
:respawn:PlayerID:RespawnCoordinates
Example -- :respawn:1:'-472.0 -1240.0 -150.0'

### Kill
:kill:KillType:PlayerID_KILLER:PlayerID_VICTIM:WEAPONID_or_ENVIRONMENTTYPE:items=WeaponID_KILLER:victimitems=WeaponID_VICTIM:KillingCoordinates

Example
:kill:frag:2:1:type=2:items=2-1:victimitems=2-1:'-482.6 -1249.2 -168.0'
:kill:suicide:1:1:type=522:items=10-1:'808.1 -551.5 -88.0'
:kill:accident:1:1:type=HURTTRIGGER:items=2-1:'-880.9 -509.4 164.7'
:kill:accident:1:1:type=LAVA:items=10-1:'1527.8 -793.1 472.0'

### Heal
:heal:PlayerID:HealQuantity:PlayerCoordinates
Example -- :heal:1:100:'112.0 142.0  -8.0'

### Armor
:armor:PlayerID:ArmorQuantity:PlayerCoordinates
Example -- :armor:1:5:'-785.0 -695.3 264.0'

### Strength Power (Begin)
:str_begin:PlayerID:PlayerCoordinates
Example -- :str_begin:1:'1868.0 -364.1 648.0'

### Strength Power (End)
:str_end:PlayerID:PlayerCoordinates
Example -- :str_end:1:'1602.8 -364.6 524.0'

### Part (Leave)
:part:PlayerID
Example -- :part:1

--------------- 
## End of Match Statistics

### Labels (ID Information for Parsing)
Example
:labels:player:score!!,,,,,,,,deaths<,dmg,dmgtaken<,,,elo,,fps,,,,,,,,,,,,,kills,,,,,,,,,,,,,,suicides<,,teamkills<


### Player (Individual Player Statistics)
Example
:player:see-labels:2,0,0,0,0,0,0,0,0,226.743286,0,0,0,-2,0,200,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0:148:-1:1:Worship101
:player:see-labels:0,0,0,0,0,0,0,0,2,0,226.743286,0,0,-2,0,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0:135:-1:2:Worship101

---------------

## Weapon ID References
1 = Blaster (Default Key Bind: 1)
2 = Shotgun (Default Key Bind: 2)
3 = Machine Gun (Default Key Bind: 3)
4 = Mortar (Grenade Launcher) (Default Key Bind: 4)
6 = Electro (Default Key Bind: 5)
7 = Crylink (Default Key Bind: 6)
8 = Vortex (Sniper) (Default Key Bind: 7)
9 = Hagar (Default Key Bind: 8)
10 = Devastator (Rocket Launcher) (Default Key Bind: 9)

## Item ID References
1 = Healing Item
2 = Armor Item
3 = Shotgun Ammo
4 = Machine Gun Ammo
5 = Mortar / Devastator Ammo
6 = Electro / Crylink / Vortex (Sniper) Ammo

# Notes
This code has been edited from https://gitlab.com/xonotic/xonotic-data.pk3dir repository at revision `d4bf1b512a56e647007f5a35d68613b5b1121dd3`.

Below are the changes made by UniGe:

Modified files:
server/client.qc
qcsrc/server/g_damage.qc
qcsrc/server/miscfunctions.qc
qcsrc/server/resources.qc 
qcsrc/server/weapons/common.qc
qcsrc/server/weapons/weaponsystem.qc

New files:
qcsrc/server/liblsl64.dll
qcsrc/server/lsl_c.qh